
var loadState = {
    preload: function () {
    
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 150,
    'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite( game.width/2, 200, 'progressBar');
    progressBar.anchor.setTo(0.5, 0.5);
    
    game.load.setPreloadSprite(progressBar,0);

    game.load.spritesheet('player', 'assets/player.png',32,32);
    game.load.image('ceiling', 'assets/ceiling.png');
    game.load.image('normal', 'assets/normal.png');
    game.load.image('nails', 'assets/nails.png');
    game.load.image('heal', 'assets/heal.png');
    game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png',96,16);
    game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
    game.load.image('spike', 'assets/spike.png');
    game.load.spritesheet('fake', 'assets/fake.png', 96, 36);

    game.load.image('background', 'assets/background.jpg');
    game.load.image('floor','assets/floor.png');

    // audio
    game.load.audio('hurt', ['assets/hurt.mp3']);
    game.load.audio('gameover', ['assets/gameover.mp3']);
    game.load.audio('bgm', ['assets/bgm.wav']);
    game.load.audio('tram', ['assets/trampoline.wav']);
    game.load.audio('heal', ['assets/heal.wav']);
    game.load.audio('break', ['assets/break.wav']);
    },
    create: function() {
    // Go to the menu state
    game.state.start('menu');
    }
};
        