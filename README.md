# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## Game process
    load->menu->play->highscore

## Rules
* Player will die if he touch ceiling or floor.
* Use arrow keys to play.

## Different platforms
* Normal
    Normal platform - no special effect

* Spike
    player will lose one health when this platform is stepped.

* Fake
    Will disappear when stepped.

* Trampoline
    Bounce when stepped.

* conveyor belt
    change player velocity

## correct physics properties and behaviors

## sound effects
    background music, gameover sound, and different sound effect for stepping different platform.

## leader board
    Leader board using firebase real-time data base. Store player's name and score. Show top 3 high score.

## other features.
    Progressive difficulty. When score reach 20, screen shake and spikes start falling down.
    When the spike collide with player, player lose one health and spike is destroyed.
    When score reach 30 there is another screen shake and platform start moving.


## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  done  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  done  |
|         All things in your game should have correct physical properties and behaviors.         |  done  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  done  |
| Add some additional sound effects and UI to enrich your game.                                  |  done  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  done  |
| Appearance (subjective)                                                                        |  done  |
| Other creative features in your game (describe on README.md)                                   |  done  |


