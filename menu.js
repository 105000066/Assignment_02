
var menuState = {
    preload:function(){
        
    },
    create: function() {
    game.stage.backgroundColor = '#000000';
    var nameLabel = game.add.text(game.width/2, 80, '小朋友下樓梯',
    { font: '50px Arial', fill: '#ffffff' });
    nameLabel.anchor.setTo(0.5, 0.5);

    // Explain how to start the game
    var startLabel = game.add.text(game.width/2, game.height-80,
    'press the up arrow key to start', { font: '25px Arial', fill: '#ffffff' });
    startLabel.anchor.setTo(0.5, 0.5);
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    upKey.onDown.add(this.start, this);
    },
    start: function() {
    // Start the actual game
    game.state.start('play');
    },
    };