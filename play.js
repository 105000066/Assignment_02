var status = 'running';
var distance = 0;
var spike_lasttime = 0;
var lasttime = 0;
var touch_spike = false;
var input_done = 0;
var score_load_done = 0;
var touch_fake = false;
var touch_rain = false;

var playState = {
    preload: function() {
        var n=0;
		firebase.database().ref("leaderboard").once('value').then(function(ev){
			ev.forEach(function(v){
				var na=v.val().player_name;
				var po=v.val().player_score;
                highscore[n++]=[String(na),Number(po)];
                score_load_done = 1;
			});
		});
    },
    create: function() {
        this.timeDelay = 0;
        game.add.image(0, 0, 'background');
        // music
        this.hurt = game.add.audio('hurt');
        this.gameover = game.add.audio('gameover');
        this.tram = game.add.audio('tram');
        this.heal = game.add.audio('heal');
        this.break = game.add.audio('break');
        this.bgmusic = new Phaser.Sound(game,'bgm',1,true);
        this.bgmusic.play();

        this.platforms = [];
        game.physics.arcade.enable(this.platforms);

        this.spikes = [];
        game.physics.arcade.enable(this.spikes);
        //keyboard control
        this.cursor = game.input.keyboard.createCursorKeys();
        
        // environment
        game.stage.backgroundColor = '#000000'; // Set background
        game.renderer.renderSession.roundPixels = true; // Setup renderer
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.ceiling = game.add.image(0,0,'ceiling');
        this.floor = game.add.sprite(0,390,'floor');


        this.player = game.add.sprite(200, 50, 'player');
        game.physics.arcade.enable(this.player);    // Enable Physics

        this.player.body.gravity.y = 500;
        this.player.animations.add('left', [0, 1, 2, 3], 8);
        this.player.animations.add('right', [9, 10, 11, 12], 8);
        this.player.animations.add('jumpleft', [18, 19, 20, 21], 12);
        this.player.animations.add('jumpright', [27, 28, 29, 30], 12);
        this.player.animations.add('jumpplayer', [36, 37, 38, 39], 12);
        this.player.life = 10;
        this.player.time = 0;
        
        this.player.body.collideWorldBounds=true;

        // text
        var style = {fill: '#ffffff', fontSize: '20px'}
        this.life = game.add.text(10, 10, '', style);
        this.score = game.add.text(10, 30, '', style);
        this.yourscore = game.add.text(260,250, '',style);


        this.name1 = game.add.text(110,130,'',style);
        this.name2 = game.add.text(110,160,'',style);
        this.name3 = game.add.text(110,190,'',style);

        this.score1 = game.add.text(260,130,'',style);
        this.score2 = game.add.text(260,160,'',style);
        this.score3 = game.add.text(260,190,'',style);
        
        this.title = game.add.text(110,100,'High Score', style);
        this.you = game.add.text(110, 250, 'You: ',style);
        
        this.over = game.add.text(110, 280, 'Press Up to return', style);

        this.title.visible = false;
        this.you.visible = false;
        this.yourscore.visible = false;
        this.over.visible = false; 
        this.name1.visible = false; 
        this.score1.visible = false; 
        this.name2.visible = false; 
        this.score2.visible = false; 
        this.name3.visible = false; 
        this.score3.visible = false;         

    },

    update: function() {
        if(distance == 20) game.camera.shake(0.05, 500);
        if(distance == 30) game.camera.shake(0.05, 500);

        if(status == 'gameover' && input_done == 0)
        {
            var name=prompt("Enter your name");
            if(name)
                firebase.database().ref("leaderboard").push({player_name:name, player_score: distance.toString()});
            input_done= 1;

        }

        if(this.cursor.up.isDown && status == 'gameover')
        {            
            
            score_load_done = 0;
            status = 'running';
            distance = 0;
            lasttime = 0;
            spike_lasttime = 0;
            touch_spike = false;
            touch_rain = false;
            this.player.life = 10;
            this.player.time = 0;
            game.state.start('menu');
            input_done = 0;

        }

        if( status != 'running') return;

        this.physics.arcade.collide( this.player, this.platforms , this.effect,null, this);
        this.physics.arcade.collide( this.player, this.spikes , this.effect_spike,null, this);
        // gameover
        if(this.player.life <=0 || checkOverlap(this.player,this.floor) || checkOverlap(this.player, this.ceiling))
        {
            this.gameover.play();
            this.GAMEOVER();
        }

        // move player
        if(this.cursor.left.isDown)
            this.player.body.velocity.x = -200;
        else if(this.cursor.right.isDown)
            this.player.body.velocity.x = 200;
        else this.player.body.velocity.x = 0;
        playeranimate(this.player);

        // updateplatform

        for(var i=0; i<this.platforms.length; i++) {
            var platform = this.platforms[i];
            platform.body.position.y -= 2;
            if( distance > 30){
            if(i%2 == 1 )platform.body.position.x += 2;
            else platform.body.position.x -= 2;
            }
            if(platform.body.position.y <= -100 || platform.body.position.x <= -20 || platform.body.position.x >= 500) {
                platform.destroy();
                this.platforms.splice(i, 1);
            }
        }
        //update spike
        for( var i=0 ; i<this.spikes.length; i++)
        {
            var spike = this.spikes[i];
            spike.body.position.y += 2;
            if( spike.body.position.y > 500)
            {
                spike.destroy();
                this.spikes.splice(i,1);
            }
        }

        // textboard
        this.life.setText('life:' + this.player.life);
        this.score.setText('Score: ' + distance);
        this.yourscore.setText(distance);

        for(var i=highscore.length;i<5;++i){
			highscore[i]=['####',0];
        }
      
        highscore.sort(function(a,b){
			return a[1]<b[1];
        });

        this.name1.setText(highscore[0][0]);
		this.score1.setText(highscore[0][1]);
		this.name2.setText(highscore[1][0]);
		this.score2.setText(highscore[1][1]);
		this.name3.setText(highscore[2][0]);
		this.score3.setText(highscore[2][1]);

        //createplatform
        if(game.time.now > lasttime + 600) {
            lasttime = game.time.now;
            this.createOnePlatform();
            distance += 1;
        }

        if(game.time.now > spike_lasttime +1000 && distance > 20) {
            spike_lasttime = game.time.now;
            this.createspike();
        }


    },

    createspike: function(){
        var spike;
        var x = Math.random()*(300) + 20;
        var y = -20;
        spike = game.add.sprite(x,y,'spike');
        this.spikes.push(spike);
        game.physics.arcade.enable(spike);
        spike.enableBody = true;
        spike.body.checkCollision.down = false;
        spike.body.checkCollision.up = false;
        spike.body.checkCollision.left = false;
        spike.body.checkCollision.right = false;
    },

    createOnePlatform: function () {

        var platform;
        var x = Math.random()*(250) + 20;
        var y = 400;
        var rand = Math.random() * 100;
                    
        if(rand < 30) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if(rand <60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        }
        
        else if (rand < 70) {
			platform = game.add.sprite(x, y, 'trampoline');
			platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
			platform.frame = 3;
        }
        else if (rand < 80) {
			platform = game.add.sprite(x, y, 'heal');
		}
		else {
			platform = game.add.sprite(x, y, 'fake');
		}
        
        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platform.enableBody = true;
        this.platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    GAMEOVER: function(){
        
        this.platforms.forEach(function(s){s.destroy()});
        this.spikes.forEach(function(s){s.destroy()});
        this.spikes = [];
        this.platforms = [];
        status = 'gameover';
        this.title.visible = true;
        this.you.visible = true;
        this.yourscore.visible = true;
        this.over.visible = true; 
        this.name1.visible = true; 
        this.score1.visible = true; 
        this.name2.visible = true; 
        this.score2.visible = true; 
        this.name3.visible = true; 
        this.score3.visible = true; 

        this.player.body.position.x = 200;
        this.player.body.position.y = 50;
        this.player.body.gravity = 0;
    },

    effect_spike: function(player , spike)
    {
            game.camera.flash(0xff0000, 100);
            this.hurt.play();
            player.life -= 1;
            for( var i=0; i< this.spikes.length ; i++)
            {
                if( this.spikes[i] == spike)
                {
                    spike.destroy();
                    this.spikes.splice(i, 1);
                    break;
                }    
            }
    },

    effect: function(player, platform) {
        if(platform.key == 'conveyorRight') {
            player.body.x += 2;

        }
        if(platform.key == 'conveyorLeft') {
            player.body.x -= 2;

        }
        if(platform.key == 'nails') {
            if (!touch_spike) {
                player.life -= 1;
                touch_spike = true;
                game.camera.flash(0xff0000, 100);
                this.hurt.play();
            }
        }

        if(platform.key == 'heal')
        {
            if(!touch_spike)
            {
                player.life += 1;
                touch_spike = true;
                game.camera.flash( 0xff69b4 , 100)
                this.heal.play();
             }
        }

        if(platform.key == 'trampoline') {
            if (!touch_spike) 
            {
                platform.animations.play('jump');
                touch_spike = true;
                this.tram.play();
            }
            player.body.velocity.y = -200;
        }
        
        if(platform.key == 'fake')
        {
            platform.body.immovable = false;
            platform.enableBody = false;
            this.break.play();
            var i;
            for(i=0; i< this.platforms.length ; i++)
            {
                if( this.platforms[i] == platform)
                {
                    platform.destroy();
                    this.platforms.splice(i, 1);
                    break;
                }    
            }
        }
    }
};


function playeranimate(player)
{
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;
    if( y!= 0)
        touch_spike = false;
    if (x < 0 && y > 0) {
        player.animations.play('jumpleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('jumpright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('jumpplayer');
    }
    if (x == 0 && y == 0) {
      player.frame = 8;
    }
}



function checkOverlap(spriteA, spriteB) {

    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();

    return Phaser.Rectangle.intersects(boundsA, boundsB);

}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
  }